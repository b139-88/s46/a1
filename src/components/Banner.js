import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import { Container, Row, Col } from 'react-bootstrap';
import { useHistory } from "react-router-dom";

export default function Banner({bannerProps}) {
    const {title, description, button, url} = bannerProps;
    let history = useHistory();
    
    function goToUrl(){
        history.push(url);
    }

    return(
        <Container>
            <Row className="justify-content-center">
                <Col xs={10} md={8}>
                    <Jumbotron>
                        <h1>{title}</h1>
                        <p>
                            {description}
                        </p>
                        <p>
                            <Button variant="primary" onClick={goToUrl}>{button}</Button>
                        </p>
                    </Jumbotron>
                </Col>
            </Row>
        </Container>
    );
}