import { NavLink, Link } from "react-router-dom";
import { Nav, Navbar} from "react-bootstrap";
import { useContext, Fragment } from "react";
import UserContext from '../UserContext';

function AppNavBar() {
    const { user } = useContext(UserContext);

    let leftNav = user.id != null ?
        <Fragment>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
        </Fragment> 
    :
        <Fragment>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
        </Fragment> 

    return(
        <Navbar bg="primary" expand="lg">
            <Navbar.Brand as={Link} to="/">React Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                </Nav>
                <Nav>
                    {leftNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default AppNavBar;