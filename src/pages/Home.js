import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import  { Fragment } from 'react';

function Home() {

    let bannerMessage = {
        title: 'Welcome to React-Booking App!',
        description: 'Opportunities for everyone, everywhere!',
        button: 'Enroll',
        url: '/enroll'
    };

    return (
        <Fragment>
            <Banner bannerProps={bannerMessage}/>
            <Highlights />
        </Fragment>
    );
}

export default Home;