import { useState, useEffect, useContext } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";
import UserContext from '../UserContext';

function Register() {
    let history = useHistory();
    const { user } = useContext(UserContext);
    
    if (user.id != null) {
        history.push('/courses');
    }

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [cp, setCp] = useState("");

    const [isDisabled, setIsDisabled] = useState(true);

    function registerUser(e) {
        e.preventDefault();

        fetch("http://127.0.0.1:4000/api/users/register", {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                password: password,
                email: email,
                mobileNo: mobileNumber,
            })
        })
        .then(response => response.json())
        .then(data => {
            if (!data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "Email already exists!"
                });
            } else {
                Swal.fire({
                    icon: 'success',
                    title: 'Yay!',
                    text: "You have been registered successfully!"
                });

                history.push('/login');
            }
        });

        setEmail("");
        setPassword("");
        setCp("");
    }

    useEffect(() => {
        if ((firstName !== "" && lastName !== "" && mobileNumber !== "" && email !== "" && password !== "" && cp !== "") && password === cp) {
            setIsDisabled(false);
        }
    }, [firstName, lastName, mobileNumber, email, password, cp]);

    return (
        <Container className="mb-5">
            <Row className="justify-content-center">
                <Col xs={10} md={6}>
                    <Form onSubmit={(e) => registerUser(e)}>
                        <h1>Register</h1>
                        <Form.Group controlId="formBasicFirstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="text"
                            value={firstName}
                            onChange={(e) =>
                                setFirstName(e.target.value)
                            }
                            placeholder="Enter first name" />
                        </Form.Group>

                        <Form.Group controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="text"
                            value={lastName}
                            onChange={(e) =>
                                setLastName(e.target.value)
                            }
                            placeholder="Enter last name" />
                        </Form.Group>

                        <Form.Group controlId="formBasicMobileNumber">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control
                            type="text"
                            value={mobileNumber}
                            onChange={(e) =>
                                setMobileNumber(e.target.value)
                            }
                            placeholder="09061234567" />
                        </Form.Group>
                        
                        <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            value={email}
                            onChange={(e) =>
                                setEmail(e.target.value)
                            }
                            placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                        type="password" 
                        value={password}
                        onChange={(e) =>
                            setPassword(e.target.value)
                        }
                        placeholder="Password" />
                        </Form.Group>

                        <Form.Group controlId="formBasicConfirmPassword">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control
                        type="password" 
                        value={cp}
                        onChange={(e) =>
                            setCp(e.target.value)
                        }
                        placeholder="Confirm Password" />
                        </Form.Group>

                        <Button variant="primary" disabled={isDisabled} type="submit">Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default Register;
