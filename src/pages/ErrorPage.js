import Banner from '../components/Banner';
import  { Fragment } from 'react';

function ErrorPage() {

    let bannerMessage = {
        title: '404 - Not Found',
        description: 'The page you are looking for cannot be found.',
        button: 'Go to homepage',
        url: '/'
    };

    return (
        <Fragment>
            <Banner bannerProps={bannerMessage}/>
        </Fragment>
    );
}

export default ErrorPage;