import { useState, useEffect, useContext } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from '../UserContext';

function Login() {
    let history = useHistory();

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [isDisabled, setIsDisabled] = useState(true);

    function loginUser(e) {
        e.preventDefault();

        fetch("http://127.0.0.1:4000/api/users/login", {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            if (data !== "undefined"){
                localStorage.setItem('token', data.access);
            }
        });

        userDetails();

        setEmail("");
        setPassword("");

        <Redirect to="/courses" />
    }

    const userDetails = () => {
        fetch("http://127.0.0.1:4000/api/users/details", {
            method: 'GET',
            headers: {
                contentType: "application/json",
                authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        });
    };

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

    return (
        (user.id != null) ?
        <Redirect to="/courses" />
        :
        <Container>
            <Row className="justify-content-center">
                <Col xs={10} md={6}>
                    <Form onSubmit={(e) => loginUser(e)}>
                        <h1>Login</h1>
                        <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            value={email}
                            onChange={(e) =>
                                setEmail(e.target.value)
                            }
                            placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                        type="password" 
                        value={password}
                        onChange={(e) =>
                            setPassword(e.target.value)
                        }
                        placeholder="Password" />
                        </Form.Group>

                        <Button variant="primary" disabled={isDisabled} type="submit">Login</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default Login;
