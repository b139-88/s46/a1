import './App.css';
import { useState, useEffect } from "react";
import { Container } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import { UserProvider } from './UserContext';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null,
    });
  };

  useEffect(() => {
    fetch("http://127.0.0.1:4000/api/users/details", {
      method: 'GET',
      headers: {
          contentType: "application/json",
          authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {
      if (data) {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null,
        });
      }
    });
  }, []);
  
  return(
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container fluid className="m-3">
          <Switch>
              {/* Home */}
              <Route exact path="/" component={Home} />
              {/* Courses  */}
              <Route exact path="/courses" component={Courses} />
              {/* Register  */}
              <Route exact path="/register" component={Register} />
              {/* Login  */}
              <Route exact path="/login" component={Login} />
              {/* Logout  */}
              <Route exact path="/logout" component={Logout} />
              {/* 404 Pages */}
              <Route component={ErrorPage} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  )
    
}


export default App;
